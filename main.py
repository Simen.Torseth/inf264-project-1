from __future__ import annotations

import time

import numpy as np
import pandas as pd
from sklearn import model_selection


class Node:
    """
    A node object that stores a feature, a split value, and 2 child nodes
    """
    def __init__(self, feature=None, split_value=None, left=None, right=None):
        self.label = None
        self.feature = feature
        self.split_value = split_value
        self.left = left
        self.right = right

    def set_left(self, node):
        self.left = node

    def set_right(self, node):
        self.right = node

    def set_label(self, label):
        self.label = label

    def prune(self):
        self.left = None
        self.right = None

    def is_leaf(self):
        return self.left is None and self.right is None \
               and self.feature is None and self.split_value is None and self.label is not None

    def display(self):
        """
        @author: J. V. https://stackoverflow.com/users/1143396/j-v
        Displays the decision tree
        """
        lines, *_ = self.display_helper()
        for line in lines:
            print(line)

    def display_helper(self):
        """
        @author: J. V. https://stackoverflow.com/users/1143396/j-v
        Credit to J. V. for creating a binary tree display method. It's only used for
        debugging purposes.
        Retrieved from url: https://stackoverflow.com/questions/34012886/print-binary-tree-level-by-level-in-python
        :return: list of strings, width, height, and horizontal coordinate of the root.
        """
        # No children
        if self.right is None and self.left is None:
            line = '%s' % self.label
            width = len(line)
            height = 1
            middle = width // 2
            return [line], width, height, middle
        # Only a left child
        if self.right is None:
            lines, n, p, x = self.left.display_helper()
            s = '%s' % self.feature
            u = len(s)
            first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s
            second_line = x * ' ' + '/' + (n - x - 1 + u) * ' '
            shifted_lines = [line + u * ' ' for line in lines]
            return [first_line, second_line] + shifted_lines, n + u, p + 2, n + u // 2
        # Only a right child.
        if self.left is None:
            lines, n, p, x = self.right.display_helper()
            s = '%s' % self.feature
            u = len(s)
            first_line = s + x * '_' + (n - x) * ' '
            second_line = (u + x) * ' ' + '\\' + (n - x - 1) * ' '
            shifted_lines = [u * ' ' + line for line in lines]
            return [first_line, second_line] + shifted_lines, n + u, p + 2, u // 2
        # Two children
        left, n, p, x = self.left.display_helper()
        right, m, q, y = self.right.display_helper()
        s = '%s' % self.feature
        u = len(s)
        first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s + y * '_' + (m - y) * ' '
        second_line = x * ' ' + '/' + (n - x - 1 + u + y) * ' ' + '\\' + (m - y - 1) * ' '
        if p < q:
            left += [n * ' '] * (q - p)
        elif q < p:
            right += [m * ' '] * (p - q)
        zipped_lines = zip(left, right)
        lines = [first_line, second_line] + [a + u * ' ' + b for a, b in zipped_lines]
        return lines, n + m + u, max(p, q) + 2, n + u // 2


def label_probability(labels: pd.Series | pd.DataFrame) -> np.ndarray:
    """
    Calculates the probability of each label
    :param labels: an array of labels
    :return: ndarrary with the probabilities of g and h
    """
    if type(labels) is pd.Series:
        amounts = labels.value_counts()
    elif type(labels) is pd.DataFrame:
        amounts = labels.value_counts(["label"])
    else:
        raise TypeError(f"Array is not of the correct type. labels: {labels}")
    # calculate the probabilities for each label
    if 'g' in amounts.index and 'h' in amounts.index:
        prob_g = amounts['g'] / len(labels)
        prob_h = amounts['h'] / len(labels)
        return np.array([prob_g, prob_h])
    elif 'g' in amounts.index and 'h' not in amounts.index:
        return np.array([1, 0])
    else:
        return np.array([0, 1])


def split_at_feature(matrix: pd.DataFrame, labels: pd.Series, feature: int) -> tuple[pd.DataFrame, pd.DataFrame, float]:
    """
    Splits the matrix at a certain feature using mean as the split value
    :return: matrix split at mean
    """
    # copy the matrix, so that we don't make changes
    matrix_copy = matrix.copy()
    # find the mean of given feature
    mean = matrix_copy[feature].mean()
    # concatenate labels
    matrix_copy.insert(len(matrix_copy.columns), "label", labels)
    vals_greater = matrix_copy.loc[matrix_copy[feature] >= mean]
    vals_less = matrix_copy.loc[matrix_copy[feature] < mean]

    return vals_greater, vals_less, mean


def entropy(label_prob: np.ndarray) -> float:
    """
    Calculates the average information content of a random variable according
    to Parviainen, P. (2022, Sep. 8). Decision Trees[Powerpoint slide 10]
    :return: entropy
    """
    g = label_prob[0]
    h = label_prob[1]
    if g > 0 and h > 0:
        return -(g * np.log2(g) + h * np.log2(h))
    return 0


def gini(label_prob: np.ndarray) -> float:
    """
    Calculates the average information content of a random variable according
    to Parviainen, P. (2022, Sep. 8). Decision Trees[Powerpoint slide 17]
    :return: gini impurity
    """
    g = label_prob[0]
    h = label_prob[1]
    if g > 0 and h > 0:
        return g * h + h * g
    return 0


def conditional_entropy(greater, less, matrix) -> float:
    return entropy(label_probability(greater)) * (len(greater) / len(matrix)) + \
           entropy(label_probability(less)) * (len(less) / len(matrix))


def information_gain(features: pd.DataFrame, labels: pd.Series, impurity_measure: str) -> int:
    """
    Calculates the information gain for each column using the mean as a split threshold
    :return: maximum information gain
    """
    if impurity_measure not in ["entropy", "gini"]:
        raise ValueError(f"Impurity measure doesn't exist. impurity_measure: {impurity_measure}")
    gains = []
    for i in range(len(features.columns)):
        # information gain formula according to Parviainen, P. (2022, Sep. 8).
        # Decision Trees[Powerpoint slide 22]
        greater, less, _ = split_at_feature(features, labels, i)
        if impurity_measure == "entropy":
            gain = entropy(label_probability(labels)) - conditional_entropy(greater, less, features)
            gains.append(gain)
        elif impurity_measure == "gini":
            gain = gini(label_probability(labels))
            gains.append(gain)
    max_gain = max(gains)
    return gains.index(max_gain)


def identical_features(matrix: pd.DataFrame) -> bool:
    """
    Checks if all values are equal for each feature
    """
    temp = matrix.to_numpy()
    # for every column, check if first element is equal to rest of the column
    truth = (temp[0] == temp).all(0)
    # check if all booleans are the same
    return (truth == True).all()  # dodgy comparison hehe


def learn(matrix: pd.DataFrame, labels: pd.Series, impurity_measure: str = "entropy"):
    """
    Creates a decision tree from a training matrix using an impurity measure
    :return: a decision tree
    """
    if impurity_measure not in ["entropy", "gini"]:
        raise ValueError(f"Impurity measure doesn't exist. impurity_measure: {impurity_measure}")
    # convert to a set for easier checking
    if len(set(labels)) == 1:
        leaf = Node()
        leaf.set_label(labels.iloc[0])
        return leaf
    elif identical_features(matrix):
        label = labels.value_counts().idxmax()
        leaf = Node()
        leaf.set_label(label)
        return leaf
    else:
        index = information_gain(matrix, labels, impurity_measure)
        left, right, mean = split_at_feature(matrix, labels, index)
        node = Node(index, mean)
        left_node = learn(left.iloc[:, :-1], left["label"])
        right_node = learn(right.iloc[:, :-1], right["label"])
        node.set_left(left_node)
        node.set_right(right_node)
        return node


def predict(data_point: np.ndarray, tree: Node) -> str:
    """
    Predicts the label of a datapoint using a decision tree model
    :return: a leaf node with a label
    """
    if tree.is_leaf():
        return tree.label
    # Decide whether we go left or right in the tree
    if data_point[tree.feature] >= tree.split_value:
        leaf = predict(data_point, tree.left)
    else:
        leaf = predict(data_point, tree.right)
    return leaf


def amount_correct(matrix: pd.DataFrame, labels: pd.Series, tree: Node) -> float:
    """
    Calculates accuracy of decision tree model
    :return: percentage of correctly guessed data points
    """
    corr_count = 0
    for i in range(len(matrix)):
        prediction = predict(matrix.to_numpy()[i], tree)
        if labels[i] == prediction:
            corr_count += 1
    percentage = corr_count / len(matrix)
    return percentage


def main():
    # read the file "magic04.data" as a csv file
    data = pd.read_csv("magic04.data", header=None)
    # make headers for each column and set the headers
    columns = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "label"]
    data.columns = columns
    # split the data into features and target labels
    features = data.iloc[:, :-1]
    labels = data.iloc[:, -1]
    # set the seed, and make train, validation and test sets into ratio of 0.7 and 0.3
    seed = 1337
    features_train, features_val_test, \
    labels_train, labels_val_test = model_selection.train_test_split(features, labels, test_size=0.3, random_state=seed)
    # shuffle and split the data into validation and test sets with a ratio of 0.5 and 0.5
    features_val, features_test, \
        labels_val, labels_test = model_selection.train_test_split(features, labels, test_size=0.5, random_state=seed)

    # reset the index of the dataframes for comparing indices
    features_train.reset_index(drop=True, inplace=True)
    labels_train.reset_index(drop=True, inplace=True)
    features_val_test.reset_index(drop=True, inplace=True)
    labels_val_test.reset_index(drop=True, inplace=True)

    # make the decision tree
    start_ent = time.time()
    entropy_tree = learn(features_train, labels_train, impurity_measure="entropy")
    print("Done making decision tree using entropy.")
    end_ent = time.time()
    start_gini = time.time()
    gini_tree = learn(features_train, labels_train, impurity_measure="gini")
    print("Done making decision tree using gini.")
    end_gini = time.time()
    duration_ent = end_ent - start_ent
    duration_gini = end_gini - start_gini
    print(f"Elapsed time:")
    print(f"1. Entropy | {duration_ent:.1f} seconds")
    print(f"2. Gini    | {duration_gini:.1f} seconds")
    # check of what percentage the tree correctly predicted
    ent_train_fraction = amount_correct(features_train, labels_train, entropy_tree)
    ent_val_fraction = amount_correct(features_val_test, labels_val_test, entropy_tree)
    gini_train_fraction = amount_correct(features_train, labels_train, gini_tree)
    gini_val_fraction = amount_correct(features_val_test, labels_val_test, gini_tree)
    print(f"Amount of correctly labeled points:")
    print("\tEntropy:")
    print(f"\t\tTraining data: {ent_train_fraction}")
    print(f"\t\tValidation data: {ent_val_fraction}")
    print("\tGini:")
    print(f"\t\tTraining data: {gini_train_fraction}")
    print(f"\t\tValidation data: {gini_val_fraction}")


if __name__ == '__main__':
    main()
