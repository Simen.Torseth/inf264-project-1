take the average of each feature over columns

start on the label

Step by step - Greedy algo (ID3):
    if (all data points have same label)
        return a leaf with that label
    else if (all data points have identical val)
        return a leaf with the most common label
    else
        choose a feature that maximises IG
        for each val of feature
            add_branch
        for each branch
            call the algorithm recursively for data
            points with that feature


maybe add entropy attr, because it's always calculated
    